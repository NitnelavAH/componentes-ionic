import { Component, OnInit } from '@angular/core';
import {AlertController } from '@ionic/angular';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.page.html',
  styleUrls: ['./alert.page.scss'],
})
export class AlertPage implements OnInit {

  titulo:String;
  
  constructor(public alertCtrl: AlertController) {
    
  }

  ngOnInit() {
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'This is an alert message.',
      backdropDismiss: false,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Cancelar');
          }
        }, {
          text: 'Okay',
          handler: () => {

            console.log('Boton OK');
            
          }
        }
      ]
    });

    await alert.present();
  }

  async presentAlertInput() {
    const alert = await this.alertCtrl.create({
      header: 'Input',
      subHeader: '¿Cual es su nombre?',
      backdropDismiss: false,
      inputs:[
        {
          name: 'txtNombre',
          type: 'text',
          placeholder: 'Ingresa tu nombre'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Cancelar');
          }
        }, {
          text: 'Okay',
          handler: (data) => {
            this.titulo = data.txtNombre;
            console.log('Boton OK',data);
            
          }
        }
      ]
    });

    await alert.present();
  }
}
