import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Componente } from 'src/app/interfaces/interfaces';
import { Observable } from 'rxjs';
import { DataService } from '../../services/data.service';
import { Platform} from '@ionic/angular';
import {AlertController } from '@ionic/angular';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit, OnDestroy, AfterViewInit {
  componentes : Observable<Componente []>;
  backButtonSubscription; 

  constructor(private menuCtrl: MenuController, 
              private dataService : DataService,
              private platform: Platform, 
              public alertCtrl: AlertController) { 
                
              }

  ngOnInit() {
    this.componentes = this.dataService.getMenuOpts();
  }

  toggleMenu(){
      this.menuCtrl.toggle();
  }

  ngAfterViewInit() { 
    this.backButtonSubscription = this.platform.backButton.subscribe(() => {
     this.presentAlert();
    });
  }
  ngOnDestroy() {
    this.backButtonSubscription.unsubscribe();
  }


  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: '',
      subHeader: 'Salir de la APP?',
      message: '',
      backdropDismiss: true,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {

          }
        }, {
          text: 'Salir',
          handler: () => {
            navigator['app'].exitApp();
          }
        }
      ]
    });

    await alert.present();
  }


}

