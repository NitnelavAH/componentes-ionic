import { Component, OnInit } from '@angular/core';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player/ngx';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-youtube-player',
  templateUrl: './youtube-player.page.html',
  styleUrls: ['./youtube-player.page.scss'],
})
export class YoutubePlayerPage implements OnInit {

  constructor(public navCtrl: NavController, private youtube: YoutubeVideoPlayer) { }

  watch(id){
    this.youtube.openVideo(id);
  }

  ngOnInit() {
  }

}
