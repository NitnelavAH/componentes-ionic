import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Observable } from 'rxjs';
import { IonList, ToastController } from '@ionic/angular';


@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

  @ViewChild('lista') lista: IonList;
  
  usuarios: Observable<any>;

  constructor(private dataSevice: DataService, 
              public toastController: ToastController) { }

  ngOnInit() {

    this.usuarios = this.dataSevice.getUsers();
  }

  
  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }


  favorite(user){
    console.log('favorite',user);
    this.presentToast('Favorite!!!');
    this.lista.closeSlidingItems();
  }
  
  share(user){
    console.log('share',user);
    this.presentToast('Share');
    this.lista.closeSlidingItems();
  }
  
  delete(user){
    console.log('delete',user);
    this.presentToast('Delete');
    this.lista.closeSlidingItems();
  }

}
